# ClamTk, copyright (C) 2004-2013 Dave M
#
# This file is part of ClamTk (http://code.google.com/p/clamtk/).
#
# ClamTk is free software; you can redistribute it and/or modify it
# under the terms of either:
#
# a) the GNU General Public License as published by the Free Software
# Foundation; either version 1, or (at your option) any later version, or
#
# b) the "Artistic License".
package ClamTk::Shortcuts;

use Glib 'TRUE', 'FALSE';

# use strict;
# use warnings;
$| = 1;

use POSIX 'locale_h';
use Locale::gettext;

sub get_pseudo_keys {
    #<<<
    my @entries = (
        [
           'About', undef,
           gettext('About'), '<control>A',
            undef, sub { ClamTk::GUI::about() },
            FALSE
        ],
        [
            'Exit', undef,
            gettext('Exit'), '<control>X',
            undef, sub { Gtk2->main_quit },
            FALSE
        ],
        [
            'Select a file', undef,
            gettext('Select a file'), '<control>F',
            undef, sub { ClamTk::GUI::select_file() },
            FALSE
        ],
        [
            'Select a directory', undef,
            gettext('Select a directory'), '<control>D',
            undef, sub { ClamTk::GUI::select_directory() },
            FALSE
        ],
    );
    #>>>

    return @entries;
}

sub get_ui_info {
    return "<ui>
                <menubar name='MenuBar'>
                 <menuitem action='About'/>
                 <menuitem action='Exit'/>
                 <menuitem action='Select a file'/>
                 <menuitem action='Select a directory'/>
                </menubar>
                </ui>";
}

1;

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public License
# as published by the Free Software Foundation; either version 2.1 of
# the License, or (at your option) any later version.
